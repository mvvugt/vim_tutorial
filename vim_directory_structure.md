# Configure vim using the `.vim` directory

Vim can be configured using by placing small snippets of code in your `.vimrc`
that will be applied in all your sessions. 

Sometimes you may want to have 'filetype' specific settings, or include large 
chunks of code - actual plugins. 
These configurations are often best places in your `.vim` directory.

## The `.vim` directory

My personal directory looks like this:

```sh
.vim
├── after
│   └── ftplugin
├── colors
├── customizations
│   └── after
├── ftdetect
├── ftplugin
├── indent
├── pack
├── plugin
├── sessions
├── spell
│   ├── python
│   ├── r
│   ├── rmarkdown
│   └── rnoweb
├── syntax
└── templates
```

### `~/.vim/after`

Any plugin or settings placed here will be applied after all the other settings
are loaded. 
Specifically here, I am loading file specific settings through `ftplugin` which will amend and extend behaviours set in `.vimrc`

### `~/.vim/colors`

Files in this directory will be interpreted as colour schemes.
For example, the colour scheme `badwolf` can be load through 
`: colorscheme badwolf`.

### `~/.vim/ftdetect`

You can tell vim which type of language you are writing code for, for example
`R`, by manually by typing `: set ft=r`.

Instead manually setting the file type each time you can simply place a file 
in `ftdetect` that will automatically set the correct file type. 
For example to automatically detect R files:

```sh
cat > r.vim
au BufNewFile,BufRead .r,.R  setf r
```

### `~/.vim/ftplugin`

Vim can load filetype specific configurations by placing your settings here. 
For example if your file type is `python`, vim will look for python specific
settings supplied in the file `python.vim`.

For example, some of my python configurations include:

```vim
" ----- Auto remove trailing spaces
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\S\zs\s\+$//e
    call winrestview(l:save)
endfun
autocmd BufWritePre * :call TrimWhitespace()

" ----- Keep indentation off empty lines
inoremap <CR> <CR>x<BS>

" -------- Colours

" highlights
let python_highlight_all=1
" indicate the 80 character line limit
highlight ColorColumn ctermbg=DarkRed
call matchadd('ColorColumn', '\%81v', 100)
setlocal colorcolumn=80

" colour scheme
set background=dark
colorscheme breezy

" --------- Spell check
"  set spell
setlocal spelllang=en_gb
setlocal spellfile=~/.vim/spell/en.utf-8.add
setlocal spellfile+=~/.vim/spell/python/func.utf-8.add

" set spell format
" cterm, the format
" ctermfg, the foreground colour
" ctermbg, the background colour
hi SpellBad cterm=underline,bold ctermfg=232 ctermbg=214
```

As mentioned at the start, I actually keep these filetype specific settings
in `~/.vim/after/ftplugin`.

### `~/.vim/syntax`

Depending on the filetype specific syntax files will be loaded which instruct
how certain parts of your code should be highlighted. 
From the vim documentation 

> Syntax highlighting enables Vim to show parts of the text in another font or
> color.	Those parts can be specific keywords or text matching a pattern.
> Vim
> doesn't parse the whole file (to keep it fast), so the highlighting has its
> limitations.  Lexical highlighting might be a better name, but since
> everybody
> calls it syntax highlighting we'll stick with that.

### ` ~/.vim/indent/`

_I should really do this as well_:

>  indent files should set options related to indentation for their filetypes,
>  and those options should be buffer-local.
> 
> Yes, you could simply put this code in the ftplugin files, but it's better 
> to separate it out so other Vim users will understand what you're doing.
> It's just a convention.

### `~/.vim/spell`

Contains (filetype-specific) spell files here. 
See `:h spell` for a detailed explanation of their usage. 

### `~/.vim/templates/`

Place code templates here that you want to load when starting a clean file. 

For example my `~/.vim/after/ftplugin/tex.vim` contains the following snippet:

```vim
" --------- load skeleton file
"  If autocmd exists 
"  Then source the script only for NewBuffers
" autocmd BufNewFile * 0r ~/.vim/templates/skeleton.tex
if has("autocmd")
    autocmd BufNewFile * 0r ~/.vim/templates/skeleton.tex
endif
```

Which loads the following LaTeX template: 

```tex
\documentclass{article}

%%%%%%%%%%%%% If we use xelatex, some packages can be skipped
\usepackage{ifxetex}

\ifxetex
    \usepackage{fontspec}
    % encoding
    \usepackage{polyglossia}
    \usepackage{xunicode}
    \usepackage{xltxtra}
\else
    \usepackage[T1]{fontenc}
    \usepackage[utf8]{inputenc}
    \usepackage{lmodern}
    % encoding
    \usepackage[english]{babel}
\fi

%%%%%%%%%%%%% packages
% package for affiliations 
\usepackage{authblk}

%%%%%%%% tables
\usepackage{booktabs}
\usepackage{longtable}
% \setlength{\extrarowheight}{3pt} %table height

%%%%%%%% figures
\usepackage{pgfplots}
\pgfplotsset{compat=1.16}

%%%%%%%% captions
\usepackage{caption}
\captionsetup[table]{name = Table, format = plain, labelfont = bf}
\captionsetup[figure]{name = Figure, format = plain, labelfont = bf,
                      justification = justified}
\def\theequation{A\arabic{equation}}

%%%%%%%% Math set-up
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{gensymb} % degree symbol
\usepackage{array}

% math control for line breaks
\binoppenalty=3000
\relpenalty=3000

% Euler's number upright not italic
\newcommand{\me}{\mathrm{e}}

%Expectations 
\newcommand{\E}[1]{\mathbb{E}\!\left( #1 \right)}
\newcommand{\V}[1]{\mathbb{V}\!\left( #1 \right)}

%%%%%%%% autobrackets
\newcommand{\autobrackR}[1]{\left(#1\right)}
\newcommand{\autobrackC}[1]{\left\{#1\right\}}
\newcommand{\autobrackS}[1]{\left[#1\right]}

%%%%%%%% References

% bibtex
\usepackage[style=numeric,
url = false,
maxnames=5,
bibencoding=utf8,
backend=bibtex,
sorting = none]{biblatex}
\bibliography{PATH/RefBibTex.bib}

%%%%%%%%%%%%% make title 
\title{Title}
\author[1]{Author1}
\author[2]{Author2}
\author[3]{Author3}

\renewcommand\Affilfont{\itshape\small}

\affil[1]{Department of 1}
\affil[2]{Department of 2}
\affil[3]{Department of 3}

\date{}

%%%%%%%%%%%%% begin document -- start typing between begin an end document
\begin{document}

%%%%%% Title
\maketitle

%%%%%% Table of content
\setcounter{tocdepth}{1} % only list section
\setcounter{secnumdepth}{0} % do not list any section numbering
\tableofcontents

%%%%%% References
\addcontentsline{toc}{section}{References} % TOC entry
\printbibliography[title={References}] % TOC titles

\end{document}
```


### `~/.vim/plugin/`

Place your plugins here. For example you could `git clone` a specific plugin 
to this directory. 

### `~/.vim/pack/`

Instead of manually installing plugins to the `~/.vim/plugin` I have installed
[pack](https://github.com/maralla/pack), which does this for you. 
See [here](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/plugins.md) for a further introduction on plugins and some I have found useful. 




