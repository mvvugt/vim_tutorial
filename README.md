# Introduction

This repository is intended to provide an overview of how to install and
configure vim (or equivalently neovim). 

Importantly, everything will be installed in your home directory, ensuring 
transportability to a non-sudo environment like high performance clusters 
(HPCs).
Everything has been test on Ubuntu and UNIX, but should work on most other 
configurations -- if not please added fixes/differences. 

## Available tutorials

- How to [install](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/install.md) vim.
- How to setup your [.vimrc](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/vimrc.md).
- How to use [plugins](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/plugins.md) + some favourites.
- How to send code to a terminal window - add [REPL](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/repl.md) functionality to vim. 
- Understand the [.vim](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/vim_directory_structure.md) directory.

## How to learn vim

In its current shape the repository will unlikely contain much information,
or even attempt to addresses, how to use vim.

Instead, the following resources/steps may be useful
(they helped me starting out):

- Type vimtutor in your terminal, and go over the tutorial -- 
repeat this one day a week for four weeks. 
- Get (preferably **buy**) a copy of [Practical Vim](https://www.amazon.co.uk/Practical-Vim-Second-Speed-Thought/dp/1680501275/ref=sr_1_1?dchild=1&keywords=practical+vim&qid=1603011812&sr=8-1) by Drew Neil -- it is absolutely brilliant.
- Google -- there is a wealth of online material on vim.
- Within vim, access the help file by typing `:h <command>`, for example `:h quit<CR>`.
- Do not use anything else for two months -- no work will get done,
but after this you will be at a decent beginners level.

In all honesty learning vim will take time, dedication, and will be very slow
and sometimes frustrating. 
After you have persisted you will be able to navigate, edit, and write codes
amazingly fast, flexible, and above all you will not want to work with 
anything else.

## The benefit of text editors compared to 'IDEs'

Personally I learned coding through Integrated Development Environment (IDEs),
such as Rstudio, or language specific text editors such TexStudio,
and similar solutions. 
While the development suits are fantastic, their major limitation, to me at 
least, is that they force language specific workflows. 
For example keyboard shortcut will be different. 
Additionally, most IDEs assume you will want to use a mouse.
Finally, IDEs work through a GUI which is typically unavailable on a HPC and 
hence you have to send commands and scripts over ssh,
instead of working natively.

Vim, and text editors like it such as Emacs, are readily (pre-) installed on 
most (UNIX) clusters, allow for a common workflow irrespective of your
project's language, and by being locally installed are fully integrated with 
the local environment. 

While this may sound trivial, I am convinced that you will think otherwise 
after _suffering_ through the very non-trivial learning curve of vim. 

## Contributions

Contributions of any kind are most welcome, particularly:

1. Textual or script corrections,
2. Useful functions or shortcuts,
3. Worked examples.

All contributors will be included in the contributors list.
